package com.epam.task04_exceptions.controller;

import com.epam.task04_exceptions.exceptions.NameNotFoundException;
import com.epam.task04_exceptions.model.Student;

import java.util.List;

public interface Controller {
    List<Student>getStudentsList();

    Student getStudent(String name) throws NameNotFoundException;

    void addStudent(String name, int age);

    void startLesson();
}
