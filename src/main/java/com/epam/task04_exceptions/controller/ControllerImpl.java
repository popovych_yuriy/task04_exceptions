package com.epam.task04_exceptions.controller;

import com.epam.task04_exceptions.exceptions.NameNotFoundException;
import com.epam.task04_exceptions.model.BusinessLogic;
import com.epam.task04_exceptions.model.Model;
import com.epam.task04_exceptions.model.Student;

import java.util.List;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public List<Student> getStudentsList() {
        return model.getStudentsList();
    }

    @Override
    public Student getStudent(String name) throws NameNotFoundException {
        return model.getStudent(name);
    }

    @Override
    public void addStudent(String name, int age) {
        model.addStudent(name, age);
    }

    @Override
    public void startLesson() {
        model.startLesson();
    }
}
