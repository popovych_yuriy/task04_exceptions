package com.epam.task04_exceptions;

import com.epam.task04_exceptions.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
