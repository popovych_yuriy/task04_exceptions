package com.epam.task04_exceptions.exceptions;

public class ListTooLargeException extends RuntimeException{
    public ListTooLargeException(String message){
        super(message);
    }
}
