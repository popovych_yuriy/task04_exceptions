package com.epam.task04_exceptions.exceptions;

public class NameNotFoundException extends Exception {

    public NameNotFoundException(String message) {
        super(message);
    }
}
