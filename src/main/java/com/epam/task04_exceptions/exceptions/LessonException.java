package com.epam.task04_exceptions.exceptions;

public class LessonException extends Exception {

    public LessonException(String message) {
        super(message);
    }
}
