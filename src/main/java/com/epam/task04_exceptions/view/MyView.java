package com.epam.task04_exceptions.view;

import com.epam.task04_exceptions.controller.Controller;
import com.epam.task04_exceptions.controller.ControllerImpl;
import com.epam.task04_exceptions.exceptions.ListTooLargeException;
import com.epam.task04_exceptions.exceptions.NameNotFoundException;
import com.epam.task04_exceptions.model.Student;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static Scanner input = new Scanner(System.in);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - Print students list");
        menu.put("2", " 2 - Find by name(Custom checked exception if input is empty or wrong)");
        menu.put("3", " 3 - Add student(Custom unchecked exception if list of students is bigger then 10)");
        menu.put("4", " 4 - Start lesson(AutoClosable class in try-with-resources block)");
        menu.put("Q", " Q - Exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton4() {
        controller.startLesson();
    }

    private void pressButton3() {
        try {
            System.out.println("Please, enter the name of the new student:");
            String name = input.nextLine();
            System.out.println("Please, enter the age of the new student:");
            int age = input.nextInt();
            System.out.println("All students: ");
            controller.addStudent(name, age);
        } catch (ListTooLargeException e) {
            e.printStackTrace();
        }
    }

    private void pressButton2() {
        try {
            System.out.println("Please, enter the name of the student:");
            String name = input.nextLine();
            Student student = controller.getStudent(name);
            System.out.println(student.toString());
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void pressButton1() {
        System.out.println("All students: ");
        for (Student student : controller.getStudentsList()) {
            System.out.println(student.toString());
        }
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                System.out.println("Wrong input!");
            }
        } while (!keyMenu.equals("Q"));
    }
}

