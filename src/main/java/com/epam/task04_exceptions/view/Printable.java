package com.epam.task04_exceptions.view;

@FunctionalInterface
public interface Printable {
    void print();
}
