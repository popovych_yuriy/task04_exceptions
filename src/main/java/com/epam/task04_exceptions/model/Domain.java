package com.epam.task04_exceptions.model;

import com.epam.task04_exceptions.exceptions.LessonException;
import com.epam.task04_exceptions.exceptions.ListTooLargeException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Domain {
    private List<Student> studentsList;
    private List<String> names = new ArrayList<>(Arrays.asList("Liam", "Noah", "William", "James", "Oliver", "Emma",
            "Olivia", "Ava", "Isabella", "Sophia"));
    Random random = new Random();

    public Domain() {
        studentsList = new ArrayList<>();
        generateStudentsList();
    }

    private void generateStudentsList() {
        for (int i = 0; i < names.size(); i++) {
            String name = names.get(i);
            int age = random.nextInt((50 - 18) + 1) + 18;
            studentsList.add(new Student(name, age));
        }
    }

    public List<Student> getStudentsList() {
        return studentsList;
    }

    public Student getStudent(String name) {
        for (Student student : studentsList) {
            if (student.getName().equals(name)) {
                return student;
            }
        }
        return null;
    }

    public void addStudent(String name, int age) {
        if (studentsList.size() >= 10) {
            throw new ListTooLargeException("List can't exceed 10 items!");
        } else {
            studentsList.add(new Student(name, age));
        }
    }

    public void startLesson() {
        try (Lesson lesson = new Lesson()) {
            System.out.println("Lesson starts!");
        } catch (LessonException e) {
            e.printStackTrace();
        }
    }
}
