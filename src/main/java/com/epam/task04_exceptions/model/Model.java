package com.epam.task04_exceptions.model;

import com.epam.task04_exceptions.exceptions.NameNotFoundException;

import java.util.List;

public interface Model {
    List<Student> getStudentsList();

    Student getStudent(String name) throws NameNotFoundException;

    void addStudent(String name, int age);

    void startLesson();
}
