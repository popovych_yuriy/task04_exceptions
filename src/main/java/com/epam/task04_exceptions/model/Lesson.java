package com.epam.task04_exceptions.model;

import com.epam.task04_exceptions.exceptions.LessonException;

public class Lesson implements AutoCloseable {

    @Override
    public void close() throws LessonException {
        throw new LessonException("Lesson exception!");
    }
}
