package com.epam.task04_exceptions.model;

import com.epam.task04_exceptions.exceptions.NameNotFoundException;

import java.util.List;

public class BusinessLogic implements Model {
    private Domain domain;

    public BusinessLogic() {
        domain = new Domain();
    }

    @Override
    public List<Student> getStudentsList() {
        return domain.getStudentsList();
    }

    @Override
    public Student getStudent(String name) throws NameNotFoundException {
        if ("".equals(name)) {
            throw new NameNotFoundException("Name is empty!");
        } else if (domain.getStudent(name) == null) {
            throw new NameNotFoundException("There are no students with this name!");
        } else {
            return domain.getStudent(name);
        }
    }

    @Override
    public void addStudent(String name, int age) {
        domain.addStudent(name, age);
    }

    @Override
    public void startLesson() {
        domain.startLesson();
    }
}
